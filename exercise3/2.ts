/**
 * The final callback(go, line 5) is expected to be called with retVal === true
 */

const myFunc = function(go: (retVal: boolean) => void): void {
    const doSomething = function(done: () => void) {
        done();
    };

    const a = {
        name: "a",
        getName: function() {
            console.log('THIS NAME: ', this.name);
            doSomething(
                function() {
                    go(this.name == "a");
                }.bind({ name: this.name }) /*modify here*/ /*end modify here*/
            );
        },
    };

    return a.getName();
};

myFunc((retVal: boolean) => {
    console.log(`RESULT: ${retVal}`);
});
