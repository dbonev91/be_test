/**
 * Modify the regular expressions to match the sentence/comment.
 */

if (
    !"This sentense does not start with a digit like 1,2,3,4".match(
        new RegExp("[A-Za-z0-9/,\\s]\\w+")
    )
) {
    throw new Error("1");
}

if (
    !"This sentense does end with a digit like 1,2,3,4".match(new RegExp("[A-Za-z0-9/,\\s]\\w+"))
) {
    throw new Error("2");
}

//Match ever m that is NOT followed by a letter from j to z
var rex = new RegExp("m(?![j-z])");
console.log('MATCHER: ', "ma".match(rex));
console.log('MZ MATCHER: ', "mz".match(rex));
if ("ma".match(rex)[0] != "m" || "mz".match(rex) != null) {
    throw new Error("3");
}
