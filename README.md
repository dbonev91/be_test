# General rules
* Please provide your code in TypeScript
    * Make sure everything is properly typed
* Have a look at __all__ files provided, as they will guide you in the right direction
* Make sure to only submit nessesary files
* You are absolutely allowed to use code from other sources, though please mark the given including a link to the source
* Please do keep track of how long the test took you
* If you need to place any remarks do so via comments in the files

# Setup Env
* `docker-compose up` and your are done :)


# Tasks
* To see the 4 tasks on this test please refer to the index.ts files within the ./exercise1-4 folders