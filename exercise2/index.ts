import { Person } from "./models/Person";
import { GenderEnum, VehicleModelEnum } from "./models/Enums";
import { Dog } from "./models/Dog";
import { Car } from "./models/Car";
import { Weighbridge } from "./models/Weighbridge";
import { Unit } from "./models/Unit";

/**
 * Exercise 2
 *
 * Please render the given scenario into code.
 *
 * - A Person named Josh(26 years old, 120 pounds, male) is the owner of his Dog Timmy(5 years, 15 pounds, male).
 * - Josh is entering his Vehicle, a Car(4 wheels, red, Smart, 3 doors), in the front seat. He is putting his Dog in the back of the car, as front seats are only to be used by Humans.
 * - Josh takes his Car to a Weighbridge, a special Scale that can only be used by Vehicles. Tim and Josh stay in the Car, the Scale displays 2500KG.
 * - After weighting the Car, including them being passenger, Josh decides to weight himself. Since the weightbridge can only be used by Vehicles he decides to go for the "all purpose" Scale next to it. The Scale displays the weight in KG.
 * - He is also weighting his Dog.
 *
 */

export default class Exercise2 {
    constructor() {
        this.run();
    }

    public run(): void {
        const personName: string = "Josh";
        const dogName: string = "Timmy";
        const josh: Person = new Person(personName, 26, 120, GenderEnum.MALE);
        const timmy: Dog = new Dog(dogName, 5, 15, GenderEnum.MALE);
        const car: Car = new Car(4, 3, 'red', VehicleModelEnum.SMART);
        car.sitOnBackSeat(timmy);
        car.sitOnFrontSeat(josh);
        const vehicleWeightbridge: Weighbridge<Car> = new Weighbridge();
        console.log(`Vehicle "${car.model}" weights ${vehicleWeightbridge.scale(car)}KG`);
        car.standFromTheCar(personName);
        console.log(`Vehicle "${car.model}" weights ${vehicleWeightbridge.scale(car)}KG`);
        car.standFromTheCar(dogName);
        console.log(`Vehicle "${car.model}" weights ${vehicleWeightbridge.scale(car)}KG`);
        const allPurposeWeightBridge: Weighbridge<Unit> = new Weighbridge();
        console.log(`${josh.name} weights ${allPurposeWeightBridge.scale(josh)}`);
        console.log(`${timmy.name} weights ${allPurposeWeightBridge.scale(timmy)}`);
    }
}
