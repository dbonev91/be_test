import { IBody } from "./IBody";

export abstract class Body implements IBody {
    weight: number;

    constructor (weight: number) {
        this.weight = weight;
    }

    getWeight (): number {
        return this.weight;
    }
}
