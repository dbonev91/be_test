import { VehicleModelEnum, SeatPlaceEnum } from "./Enums";
import { Passanger } from "./Passanger";
import { Person } from "./Person";
import { Unit } from "./Unit";
import { Body } from "./Body";

const carWeightInPuntds: number = 5420.56;

export class Car extends Body {
    wheelsCount: number;
    doorsCount: number;
    color: string;
    model: VehicleModelEnum;

    private seatCountMap: { [x: string]: number } = {
        [SeatPlaceEnum.FRONT]: 2,
        [SeatPlaceEnum.BACK]: 2,
    }

    private passangerMap: { [x: string]: Passanger } = { };

    constructor (wheelsCount: number, doorsCount: number, color: string, model: VehicleModelEnum) {
        super(carWeightInPuntds);

        this.wheelsCount = wheelsCount;
        this.doorsCount = doorsCount;
        this.color = color;
        this.model = model;

        console.log(`New car created. Model: ` +
            `"${model}", color: "${color}", doorsCount: "${doorsCount}", wheelsCount: "${wheelsCount}"`);
    }

    private sit (unit: Unit, seatPlace: SeatPlaceEnum): void {
        if (!this.seatCountMap[seatPlace]) {
            throw Error(`All the ${this.seatCountMap[seatPlace]} seats are busy. Please try to sit on the back ones.`);
        }

        if (this.passangerMap[unit.name]) {
            throw Error('This passanger is already inside the car.');
        }

        this.passangerMap[unit.name] = <Passanger>{ unit, seatPlace };

        this.seatCountMap[seatPlace]--;

        console.log(`${unit.name} seats in "${seatPlace}" seat of the car.`);
        console.log(`There still ${this.seatCountMap[seatPlace]} free "${seatPlace}" seats in the car.`);
    }

    sitOnFrontSeat (person: Person): void {
        this.sit(person, SeatPlaceEnum.FRONT);
    }

    sitOnBackSeat (unit: Unit): void {
        this.sit(unit, SeatPlaceEnum.BACK);
    }

    standFromTheCar (name: string) {
        if (this.passangerMap[name]) {
            switch (this.passangerMap[name].seatPlace) {
                case SeatPlaceEnum.FRONT:
                    this.seatCountMap[SeatPlaceEnum.FRONT]++;
                    console.log(`${name} stands from ${SeatPlaceEnum.FRONT} seat of the car.`);
                    console.log(`There are ${this.seatCountMap[SeatPlaceEnum.FRONT]} free ${SeatPlaceEnum.FRONT} seats in the car.`);
                    break;
                case SeatPlaceEnum.BACK:
                    this.seatCountMap[SeatPlaceEnum.BACK]++;
                    console.log(`${name} stands from ${SeatPlaceEnum.BACK} seat of the car.`);
                    console.log(`There are ${this.seatCountMap[SeatPlaceEnum.BACK]} free ${SeatPlaceEnum.BACK} seats in the car.`);
                    break;
                default:
                    throw Error(`Incorrect seatplace: ${this.passangerMap[name].seatPlace}`);
            }

            delete this.passangerMap[name];
        }
    }

    getWeight (): number {
        let passangersWeight: number = 0;
        const keys: string[] = Object.keys(this.passangerMap);
        const passangersCount = keys.length;

        for (let i: number = 0; i < passangersCount; i++) {
            const key: string = keys[i];
            const unit: Unit = this.passangerMap[key].unit;
            passangersWeight += unit.getWeight();
        }

        return Math.round(this.weight + passangersWeight);
    }
}
