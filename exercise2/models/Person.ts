import { Unit } from "./Unit";
import { GenderEnum } from "./Enums";

export class Person extends Unit {
    constructor (name: string, age: number, weight: number, gender: GenderEnum) {
        super(name, age, weight, gender);
        console.log(`Created new person named: ` +
            `"${name}", age: "${age}", weight: "${weight}pounds", gender: "${gender}"`);
    }
}
