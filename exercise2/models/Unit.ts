import { GenderEnum } from "./Enums";
import { Body } from "./Body";

export abstract class Unit extends Body {
    name: string;
    age: number;
    gender: GenderEnum;

    constructor (name: string, age: number, weight: number, gender: GenderEnum) {
        super(weight);
        
        this.name = name;
        this.age = age;
        this.gender = gender;
    }
}
