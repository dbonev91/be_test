export enum GenderEnum {
    MALE = 'MALE',
    FEMALE = 'FEMALE',
}

export enum VehicleModelEnum {
    SMART = 'SMART',
}

export enum SeatPlaceEnum {
    FRONT = 'FRONT',
    BACK = 'BACK',
}
