import { IBody } from "./IBody";

export class Weighbridge<T extends IBody> {
    constructor () {
        console.log('New Weighbridge created.');
    }

    private poundsToKilograms (pounds: number) {
        return Math.round(pounds * 0.45);
    }

    scale(scaled: T): number {
        const kilograms: number = this.poundsToKilograms(scaled.getWeight());
        return kilograms;
    }
}
