export interface IBody {
    weight: number;
    getWeight(): number;
}