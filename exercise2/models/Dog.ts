import { Unit } from "./Unit";
import { GenderEnum } from "./Enums";

export class Dog extends Unit {
    constructor (name: string, age: number, weight: number, gender: GenderEnum) {
        super(name, age, weight, gender);
        console.log(`Created new dog named: ` +
            `"${name}", age: "${age}", weight: "${weight}pounds", gender: "${gender}"`);
    }
}