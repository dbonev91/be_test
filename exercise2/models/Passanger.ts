import { SeatPlaceEnum } from "./Enums";
import { Unit } from "./Unit";

export interface Passanger {
    seatPlace: SeatPlaceEnum;
    unit: Unit;
}
