/**
 * Exercise 4
 *
 * Please open https://google.com in a headless browser. Once done enter "javascript" into the search field and go to the results page.
 * Follow the first non-ad result and save a screenshot of the page.
 *
 */

const { Builder, By, until } = require('selenium-webdriver');

const assert = require('assert');

let driver = new Builder().forBrowser('chrome').build();

export default class Exercise4 {
    constructor() {
        this.run().then(() => {
            console.log('SUCCESS !');
        }, (error) => {
            console.log('ERROR: ', error);
        });
    }

    public async run() {
        await driver.get('https://www.google.com');
        const query = driver.wait(until.elementLocated(By.name('q')));
        await query.sendKeys('javascript\n');
        await driver.wait(until.elementLocated(By.id('resultStatus')), 5000);
        await driver.findElement(By.css('h3.r > a')).then(async (titles) => {
            await titles[0].getText().then((text: string) => assert(text, 'javascript'))
        });
        await driver.quit();
    }
}
